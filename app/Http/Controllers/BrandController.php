<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;

class BrandController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth.admin');
  }

  function insert(Request $request)
  {
    //  Lấy dữ liệu từ form
    $name = $request->input('name');
    $description = $request->input('description');

    // Gửi sang model -> lưu vào dtaabase
    $rs = Brand::insert($name, $description);

    if ($rs == true) {
      return redirect('brands');
    }
    return "Insert thất bại!";
  }

  function viewInsert()
  {
    return view('insert', ['brands' => Brand::getAll()]);
  }

  function getAll()
  {
    return view('brands/brand', ['brands' => Brand::getAll()]);
  }

  function edit($id)
  {
    $brand = Brand::get($id);
    return view('brands.edit', ['brand' => $brand]);
  }

  function update(Request $req, $id)
  {
    $name = $req->input('name');
    $description = $req->input('description');
    $rs = Brand::updateBrand($id, $name, $description);
    if ($rs == true) {
      return redirect('brands');
    }
    return "Update thất bại!";
  }

  function destroy($id)
  {
    $rs = Brand::destroy($id);
    if ($rs == 0) return "Thất bại";
    else return redirect('brands');
  }
}
