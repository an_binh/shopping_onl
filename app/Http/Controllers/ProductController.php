<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth.admin');
  }

  function index()
  {
    return view('home', [
      'products' => DB::table('products')->paginate(8)
    ]);
  }

  function insert(Request $req)
  {
    //  Lấy dữ liệu từ form
    $name = $req->input('name');
    $price = $req->input('price');
    $quantity = $req->input('quantity');
    $id_brand = $req->input('id_brand');

    // Xử lý file ảnh 246363.png 
    $fileName = time() . "." . $req->file('image')->extension();

    // Luu ảnh vào storage 
    $req->file('image')->storeAs('public', $fileName);

    // Lấy ra đường dẫn
    $path = 'storage/' . $fileName;

    // Gửi sang model -> lưu vào dtaabase
    $rs = Product::insert($name, $price, $quantity, $path, $id_brand);

    if ($rs == true) {
      return redirect('/products');
    }
    return "Insert thất bại!";
  }

  function viewInsert()
  {
    return view('products.insert', ['brands' => Brand::getAll()]);
  }

  function getAll()
  {
    return view('products.list', ['products' => Product::getAll()]);
  }

  function edit($id)
  {
    $product = Product::get($id);
    $brands = Brand::getAll();
    return view('products.edit', ['brands' => $brands, 'product' => $product]);
  }

  function update(Request $req, $id)
  {
    $name = $req->input('name');
    $price = $req->input('price');
    $quantity = $req->input('quantity');
    $id_brand = $req->input('id_brand');
    if ($req->file('image')) {
      $fileName = time() . "." . $req->file('image')->extension();
      $req->file('image')->storeAs('public', $fileName);
      $path = 'storage/' . $fileName;
    } else {
      $path =  $req->input('old_image');
    }
    $rs = Product::updateProduct($id, $name, $price, $quantity, $path, $id_brand);
    if ($rs == true) {
      return redirect('products');
    }
    return "Update thất bại!";
  }

  function destroy($id)
  {
    $rs = Product::destroy($id);
    if ($rs == 0) return "Thất bại";
    else return redirect('products');
  }

  public function cart()
  {
    return view('cart');
  }

  public function addToCart($id)
  {
    $product = Product::findOrFail($id);

    $cart = session()->get('cart', []);

    if (isset($cart[$id])) {
      $cart[$id]['quantity']++;
    } else {
      $cart[$id] = [
        "name" => $product->name,
        "quantity" => 1,
        "price" => $product->price,
        "image" => $product->image
      ];
    }

    session()->put('cart', $cart);
    return redirect()->back()->with('success', 'Product added to cart successfully!');
  }

  public function updateCart(Request $request)
  {
    if ($request->id && $request->quantity) {
      $cart = session()->get('cart');
      $cart[$request->id]["quantity"] = $request->quantity;
      session()->put('cart', $cart);
      session()->flash('success', 'Cart updated successfully');
    }
  }

  public function remove(Request $request)
  {
    if ($request->id) {
      $cart = session()->get('cart');
      if (isset($cart[$request->id])) {
        unset($cart[$request->id]);
        session()->put('cart', $cart);
      }
      session()->flash('success', 'Product removed successfully');
    }
  }

  function search(Request $request)
  {
    $search = $request->search;
    return view('home', [
      'products' => Product::where('name', 'like', '%' . $search . '%')->orderBy('id')->paginate(8)
    ]);
  }
}
