<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AccountController extends Controller
{
  //  Tat ca cac trang trong AdminController -> deu phai duoc danh nhap
  public function __construct()
  {
    $this->middleware('auth.admin');
  }

  function index()
  {
    return redirect('products');
  }
}
