<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Customer;

use Illuminate\Http\Request;

class OrderController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth.admin');
  }

  function payment(Request $req)
  {
    $name = $req->input('name');
    $phone = $req->input('phoneNumber');
    $address = $req->input('address');
    $id = Auth::guard('admin')->id();
    $carts = $req->session()->get('cart');
    $id_customer =  Customer::get($id)[0]->id;
    $id_order = Order::insert($name, $phone, $address, $id_customer);
    foreach ($carts as $key => $cart) {
      $check = OrderDetail::insert($cart, $key, $id_order);
      if (!$check) {
        return;
      }
    }
    session()->put('cart', []);
    return true;
  }

  public function getAll()
  {
    return view('bills.list', ['bills' => Order::getAll()]);
  }
  
  public function update(Request $req)
  {
    $id = $req->id;
    $is_set_up = $req->is_set_up;
    return OrderDetail::updateBill($id, $is_set_up);
  }
}
