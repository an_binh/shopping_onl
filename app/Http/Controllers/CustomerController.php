<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class CustomerController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth.admin');
  }

  function index()
  {
    return view('home', [
      'products' => DB::table('products')->paginate(8)
    ]);
  }
}
