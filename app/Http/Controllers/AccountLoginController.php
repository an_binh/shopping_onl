<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AccountLoginController extends Controller
{
  function showLoginForm()
  {
    return view('account.auth.login');
  }

  function logout()
  {
    Auth::guard('admin')->logout();
    return redirect()->route('account.login');
  }

  function login(Request $request)
  {
    $credentials = $request->validate([
      'username' => ['required', 'email'],
      'password' => ['required'],
    ]);
    if (Auth::guard('admin')->attempt($credentials)) {
      $type_account = Auth::guard('admin')->user()->type_account;
      session()->put('type_account', $type_account);
      if ($type_account === 1) {
        return redirect()->route('admin');
      } else {
        return redirect()->route('customer');
      }
    } else {
      // Quay ve trang dang nhap kem bao loi
      return back()->withErrors([
        'error' => 'Email hoặc mật khẩu không đúng!',
      ]);
    }
  }
}
