<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Customer extends Model
{
  use HasFactory;
  static function get($id)
  {
    return DB::table('products')->where('id', $id)->get();
  }
}
