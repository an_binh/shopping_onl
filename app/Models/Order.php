<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
  use HasFactory;
  public $timestamps = true;
  static function insert($name, $phone, $address, $id_customer)
  {
    return DB::table('orders')->insertGetId([
      'name_customer' => $name,
      'phone_number' => $phone,
      'address' => $address,
      'customer_id' => $id_customer,
    ]);
  }

  static function getAll()
  {
    return DB::table('order_details')
      ->join('orders', 'orders.id', '=', 'order_details.order_id')
      ->select('orders.id','order_details.is_set_up','orders.name_customer', 'orders.address', 'orders.phone_number', 'orders.customer_id')
      ->groupBy('order_id')
      ->orderBy('orders.id', 'desc')
      ->get();
  }
}
