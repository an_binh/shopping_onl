<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class OrderDetail extends Model
{
  use HasFactory;
  public $timestamps;
  static function insert($cart, $id_product, $id_order)
  {
    return DB::table('order_details')->insert([
      'price' => $cart['price'],
      'quantity' => $cart['quantity'],
      'is_set_up' => 0,
      'product_id' => $id_product,
      'order_id'  => $id_order,
    ]);
  }
  static function updateBill($id, $is_set_up)
  {
    return DB::table('order_details')->where('order_id', $id)->update([
      'is_set_up' => ($is_set_up === '1') ? 0 : 1,
    ]);
  }
}
