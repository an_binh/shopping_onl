<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
  use HasFactory;
  static function insert($name, $price, $quantity, $image, $id_brand)
  {
    return DB::table('products')->insert([
      'name' => $name,
      'price' => $price,
      'image' => $image,
      'quantity' => $quantity,
      'id_brand' => $id_brand,
    ]);
  }

  static function getAll()
  {
    return DB::table('products')->get();
  }

  static function get($id)
  {
    return DB::table('products')->where('id', $id)->get();
  }

  static function updateProduct($id, $name, $price, $quantity, $image, $id_brand)
  {
    return DB::table('products')->where('id', $id)->update([
      'name' => $name,
      'price' => $price,
      'image' => $image,
      'quantity' => $quantity,
      'id_brand' => $id_brand,
    ]);
  }

  static function destroy($id)
  {
    return DB::table('products')->delete($id);
  }
  static function search($data)
  {
    return DB::table('products')->delete($id);
  }
}
