<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Brand extends Model
{
  use HasFactory;
  static function insert($name, $description)
  {
    return DB::table('brands')->insert([
      'name' => $name,
      'description' => $description
    ]);
  }

  static function getAll()
  {
    return DB::table('brands')->get();
  }
  
  static function get($id)
  {
    return DB::table('brands')->where('id', $id)->get();
  }

  static function updateBrand($id, $name, $description)
  {
    return DB::table('brands')->where('id', $id)->update([
      'name' => $name, 'description' => $description
    ]);
  }

  static function destroy($id)
  {
    return DB::table('brands')->delete($id);
  }
}
