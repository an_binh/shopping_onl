<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
</head>

<body>
  @if(Session::has('error'))
  {{Session::get('error')}}
  @endif
  <section class="loginpage">
    <form action="{{url('/account/login')}}" method="POST" class="log-in">
      <h1 class="title">Login</h1>
      @csrf
      <label for="log-username"></label>
      <input type="text" placeholder="Username" minlength="4" maxlength="20" id="log-username" name="username">
      <label for="log-password"></label>
      <input type="password" placeholder="Password" minlength="6" maxlength="50" id="log-password" name="password">
      <button type="submit" id="submit">Login</button>
    </form>
  </section>

</body>

</html>
<style>
  * {
    margin: 0;
    padding: 0;
  }

  body {
    background: rgb(47, 47, 47);
  }

  .log-in {
    background: rgb(67, 67, 67);
    width: 400px;
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;
    color: aliceblue;
    margin-left: 40%;
    margin-top: 10%;
  }

  .title {
    padding-top: 30px;
  }

  #log-username {
    display: block;
    text-align: center;
    border: 2px solid rgb(50, 197, 255);
    border-radius: 40px;
    padding: 11px 30px;
    width: 160px;
    background: none;
    margin: 20px auto;
    transition: 0.25s;
    color: aliceblue;
  }

  #log-password {
    display: block;
    text-align: center;
    border: 2px solid rgb(50, 197, 255);
    border-radius: 40px;
    padding: 11px 30px;
    width: 160px;
    background: none;
    margin: 20px auto;
    transition: 0.25s;
    color: aliceblue;
  }

  #submit {
    border: 2px solid rgb(17, 255, 96);
    background: rgb(17, 255, 96);
    color: aliceblue;
    padding: 11px 20px;
    border-radius: 40px;
    margin-bottom: 30px;
    width: 100px;
    transition: 0.25s;
  }

  #log-username:hover,
  #log-password:hover {
    width: 200px;
  }

  #submit:hover {
    cursor: pointer;
    width: 120px;
  }
</style>