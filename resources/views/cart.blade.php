@extends('layouts.master')

@section('title','List cart products')

@section('content')
<table id="cart" class="table table-hover table-condensed">
  <thead>
    <tr>
      <th style="width:50%">Product</th>
      <th style="width:10%">Price</th>
      <th style="width:8%">Quantity</th>
      <th style="width:22%" class="text-center">Subtotal</th>
      <th style="width:10%"></th>
    </tr>
  </thead>
  <tbody>
    @php $total = 0 @endphp
    @if(session('cart'))
    @foreach(session('cart') as $id => $details)
    @php $total += $details['price'] * $details['quantity'] @endphp
    <tr data-id="{{ $id }}">
      <td data-th="Product">
        <div class="row">
          <div class="col-sm-3 hidden-xs"><img src="{{ $details['image'] }}" width="100" height="100" class="img-responsive" /></div>
          <div class="col-sm-9">
            <h4 class="nomargin">{{ $details['name'] }}</h4>
          </div>
        </div>
      </td>
      <td data-th="Price">{{ $details['price'] }}</td>
      <td data-th="Quantity">
        <input type="number" value="{{ $details['quantity'] }}" class="form-control quantity update-cart" />
      </td>
      <td data-th="Subtotal" class="text-center">đ{{ $details['price'] * $details['quantity'] }}</td>
      <td class="actions" data-th="">
        <button class="btn btn-danger btn-sm remove-from-cart"><i class="fa fa-trash-o"></i></button>
      </td>
    </tr>
    @endforeach
    @endif
  </tbody>
  <tfoot>
    <tr>
      <td colspan="5" class="text-right">
        <h3><strong>Total đ{{ $total }}</strong></h3>
      </td>
    </tr>
    <tr>
      <td colspan="5" class="text-right">
        <a href="{{ url('/') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a>
        <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal">
          Checkout
        </button>
      </td>
    </tr>
  </tfoot>
</table>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Payment Information</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form>      
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Name:</label>
            <input type="text" required class="form-control" id="name">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label" id="">Phone number:</label>
            <input type="text" required class="form-control" id="phone-number">
          </div>
          <div class="mb-3">
            <label for="address-text" class="col-form-label">Address:</label>
            <textarea class="form-control" required id="address"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="payment">Payment</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(".update-cart").change(function(e) {
    e.preventDefault();

    var ele = $(this);

    $.ajax({
      url: '{{ route('update.cart') }}',
      method: "patch",
      data: {
        _token: '{{ csrf_token() }}',
        id: ele.parents("tr").attr("data-id"),
        quantity: ele.parents("tr").find(".quantity").val()
      },
      success: function(response) {
        window.location.reload();
      }
    });
  });

  $(".remove-from-cart").click(function(e) {
    e.preventDefault();

    var ele = $(this);

    if (confirm("Are you sure want to remove?")) {
      $.ajax({
        url: '{{ route('remove.from.cart') }}',
        method: "DELETE",
        data: {
          _token: '{{ csrf_token() }}',
          id: ele.parents("tr").attr("data-id")
        },
        success: function(response) {
          window.location.reload();
        }
      });
    }
  });
  
  $("#payment").click(function(e) {
    var name = $( "#name" ).val();
    var phoneNumber = $( "#phone-number" ).val();
    var address = $( "#address" ).val();
    // if (name || phoneNumber || address) {
    //   $.notify("Fields cannot be left blank", "error");
    // } else 
    // }
    $.ajax({
        url: '{{ route('payment') }}',
        method: "post",
        data: {
          _token: '{{ csrf_token() }}',
          name: name,
          phoneNumber: phoneNumber,
          address: address
        },
        success: function(respons) {
          if (respons) {
            $.notify("Success", "success");
            // window.location.reload();
          } else {
            $.notify("Error", "error");
          }
        },
        error: function() {
          $.notify("ajax error", "error");
        }
      });
  });
</script>
@endsection