@extends('layouts.master')

@section('title','List products')

@section('content')
<div class="container">
  <div class="">
    <div class="">
      <form action="{{ url('search') }}" method="GET" role="search">
        <div class="row height d-flex justify-content-center align-items-center" style="padding-bottom: 0px; padding-top: 15px; height: 70px; background-color: #fff;">
          <div class="col-md-8">
            <div class="search"> <i class="fa fa-search"></i> <input type="text" class="form-control" id="search" placeholder="Have a question? Ask Now"> <button class="btn btn-primary" type="submit" id="btn-search">Search</button> </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="row-centered">
    <div class="col-xs-12 col-centered">
      <hr />
      <section class="results">
        @foreach ($products as $item)
        <div class="item important" data-filter-product="1" data-filter-platform="osx" data-sort-name="A Item title" data-sort-price="9.99" data-sort-name="A Item title">
          <div class="thumb">
            <img class="shop-thumbnail" src="{{asset($item->image)}}" alt="" />
          </div>
          <div class="name">{{$item->name}}</div>
          <div class="price"><b>đ{{$item->price}}</b></div>
          <div class="menu">
            <a href="{{url('/addToCart/'.$item->id)}}">Buy Now</a>
          </div>
        </div>
        @endforeach
      </section>
      <br>
      <nav aria-label="Page navigation">
        {!! $products->links() !!}
      </nav>
    </div>
  </div>
</div>
@endsection
<style>
  @import url("https://fonts.googleapis.com/css2?family=Poppins:weight@100;200;300;400;500;600;700;800&display=swap");

  body {
    background-color: #eee;
    font-family: "Poppins", sans-serif;
    font-weight: 300
  }

  .height {
    height: 100vh
  }

  .search {
    position: relative;
    box-shadow: 0 0 40px rgba(51, 51, 51, .1)
  }

  .search input {
    height: 60px;
    text-indent: 25px;
    border: 2px solid #d6d4d4
  }

  .search input:focus {
    box-shadow: none;
    border: 1px solid blue
  }

  .search .fa-search {
    position: absolute;
    top: 20px;
    left: 16px
  }

  .search button {
    position: absolute;
    top: 5px;
    right: 5px;
    height: 50px;
    width: 110px;
    background: blue
  }

  body {
    margin: 0;
    font-family: "Square Market", Helvetica, Arial, "Hiragino Kaku Gothic Pro", "ヒラギノ角ゴ Pro W3", "メイリオ", Meiryo, "ＭＳ Ｐゴシック", sans-serif;
  }

  form {
    background: red;
    width: 100%;
  }

  .row-centered {
    text-align: center;
  }

  .col-centered {
    display: inline-block;
    float: none;
    /* reset the text-align */
    text-align: left;
    /* inline-block space fix */
    margin-right: -4px;
    text-align: center;
  }

  hr {
    border-top: 1px solid #7f868c;
    border-bottom: none;
    position: relative;
    top: 10px;
  }

  .shadow-nav {
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    transition: 0.7s;
  }

  nav .container {
    padding: 0;
  }

  .navigation {
    transition: 0.7s;
    background: #fdfdfd;
    width: 100%;
    font-weight: 700;
    height: 70px;
    position: sticky;
    top: 0;
    z-index: 99;
  }

  .navigation-left {
    float: left;
    transform: translateY(-50%);
    top: 50%;
    position: relative;
    display: block;
  }

  .row-centered img {
    width: 100%;
  }

  .row-centered.logo img {
    width: auto;
  }

  .navigation-right {
    float: right;
    transform: translateY(-50%);
    top: 50%;
    position: relative;
    display: block;
  }

  .navigation ul.nav-desktop {
    list-style: none;
    margin: 7px 0 0 0;
    padding: 0;
    display: inline-block;
  }

  .navigation ul.nav-desktop li {
    display: inline-block;
    height: 30px;
    line-height: 2;
    padding: 15px 10px;
    font-size: 16px;
  }

  .navigation ul a {
    color: #3e4348;
  }

  .hero {
    height: 400px;
    background: #eceef1;
    width: 100%;
  }

  .hero-height {
    height: 400px;
  }

  .cta {
    background-color: #49a4d5;
    color: white;
    padding: 15px 20px;
    height: 50px;
    font-size: 16px;
    line-height: 52px;
    border-radius: 4px;
    text-decoartion: none;
    transition: 0.7s;
  }

  .cta:hover {
    background: #1a6994;
    text-decoration: none;
    color: white;
    transition: 0.7s;
  }

  .hero h1 {
    margin-top: 0;
    color: #3e4348;
  }

  .centre-text {
    text-align: center;
  }

  .hero p {
    color: #3e4348;
  }

  .hero-text {
    width: 50%;
    float: left;
    transform: translateY(-50%);
    top: 50%;
    position: relative;
  }

  .hero-image {
    width: 50%;
    float: left;
    transform: translateY(-50%);
    top: 50%;
    position: relative;
  }

  .row {
    width: 100%;
    padding: 25px 15%;
    height: 200px;
    color: #3e4348;
    font-family: "Square Market", Helvetica, Arial, "Hiragino Kaku Gothic Pro", "ヒラギノ角ゴ Pro W3", "メイリオ", Meiryo, "ＭＳ Ｐゴシック", sans-serif;
  }

  .row h3 {
    font-size: 26px;
  }

  .offer-row {
    color: #3e4348;
    background: #ffff6f;
    width: 100%;
    padding: 10px 0;
  }

  .offer-button {
    color: #3e4348;
    text-decoration: none;
    border: 2px solid #3e4348;
    padding: 7px 15px;
    border-radius: 3px;
    transition: 0.7s;
  }

  .offer-button:hover {
    transition: 0.7s;
    text-decoration: none;
    color: #ffff6f;
    background: #3e4348;
  }

  .offer-row h3 {
    margin: 0;
    font-size: 1.75em;
    padding-top: 10px;
    font-weight: 700;
  }

  .column60 {
    width: 60%;
    float: left;
    position: relative;
    transform: translateY(-50%);
    top: 50%;
  }

  .column40 {
    width: 40%;
    float: left;
  }

  .column25 {
    width: 25%;
    float: left;
  }

  .column50 {
    width: 50%;
    float: left;
    position: relative;
    transform: translateY(-50%) !important;
    top: 50%;
  }

  .grey {
    background: #e4e4e4;
  }

  .centre-image img {
    display: block;
    margin: 0 auto;
  }

  .dark {
    padding: 20px 15%;
    background: #cacaca;
  }

  footer {
    width: 100%;
    background: #cacaca;
    font-size: 14px;
  }

  .footer-top {
    width: 100%;
    padding-top: 20px;
    margin-bottom: 20px;
  }

  .footer-top img {
    margin: 0 auto;
    display: block;
  }

  .footer-row {
    width: 70%;
    padding: 20px 15%;
  }

  footer ul {
    list-style: none;
    margin: 0;
    padding: 0;
    text-align: center;
  }

  footer h4 {
    margin: 0 0 15px 0;
  }

  footer ul a,
  footer ul a:hover {
    color: #3e4348;
    text-decoration: none;
  }

  footer ul li {
    margin: 0 0 5px 0;
  }

  .footer-column50 {
    width: 50%;
    float: left;
    margin-top: 10px;
    color: #3e4348;
  }

  footer .icon {
    color: #3e4348;
    font-size: 35px;
    margin: 0 5px;
  }

  .icon-div {
    text-align: right;
  }

  .copyright {
    margin-top: 0px;
    position: relative;
    bottom: 10px;
  }

  .copyright p {
    margin: 0;
  }


  .product-item img {
    margin: 0 auto;
    display: block;
  }

  .product-item {
    text-align: center;
    font-size: 16px;
    height: auto !important;
  }

  .product-link-slider {
    display: block;
    width: 100px;
    margin: 10px auto 10px auto;
    border: 2px solid #3e4348;
    padding: 5px 10px;
    color: #3e4348;
    font-size: 14px;
    border-radius: 3px;
    text-decoration: none;
    transition: 0.7s;
    background: white;
  }

  .product-item a {
    color: #3e4348;
    text-decoration: none;
  }

  .product-link-slider:hover {
    transition: 0.7s;
    background: #eceef1;
  }

  .product-location-slider {
    font-size: 14px;
    margin: 0;
  }

  .featured-products {
    width: 70%;
    margin: 0 auto;
  }

  .product-title-slider {
    margin-bottom: 0;
  }

  .product-item.equal-height.style-6 {
    background-color: white;
    border-radius: 10px;
    margin: 10px;
  }

  .cell-right {
    text-align: center;
    margin-right: 80px;
    padding-top: 35px;
    padding-bottom: 20px;
  }

  .et_right_sidebar #main-content .container:before {
    display: none;
  }

  #main-content .container {
    padding-top: 10px;
  }

  /*animation element*/

  .animation-element {
    opacity: 0;
    position: relative;
  }

  /*animation element sliding left*/

  .animation-element.slide-left {
    opacity: 0;
    -moz-transition: all 500ms linear;
    -webkit-transition: all 500ms linear;
    -o-transition: all 500ms linear;
    transition: all 500ms linear;
    -moz-transform: translate3d(-100px, 0px, 0px);
    -webkit-transform: translate3d(-100px, 0px, 0px);
    -o-transform: translate(-100px, 0px);
    -ms-transform: translate(-100px, 0px);
    transform: translate3d(-100px, 0px, 0px);

  }

  .animation-element.slide-right {
    opacity: 0;
    -moz-transition: all 500ms linear;
    -webkit-transition: all 500ms linear;
    -o-transition: all 500ms linear;
    transition: all 500ms linear;
    -moz-transform: translate3d(100px, 0px, 0px);
    -webkit-transform: translate3d(100px, 0px, 0px);
    -o-transform: translate(100px, 0px);
    -ms-transform: translate(100px, 0px);
    transform: translate3d(100px, 0px, 0px);

  }

  .animation-element.slide-left.in-view,
  .animation-element.slide-right.in-view {
    opacity: 1;
    -moz-transform: translate3d(0px, 0px, 0px);
    -webkit-transform: translate3d(0px, 0px, 0px);
    -o-transform: translate(0px, 0px);
    -ms-transform: translate(0px, 0px);
    transform: translate3d(0px, 0px, 0px);
  }

  /*animation slide left styled for testimonials*/

  .animation-element.slide-left.testimonial,
  .animation-element.slide-right.testimonial {
    float: left;
    width: 47%;
    margin: 0% 1.5% 3% 1.5%;
    background: #F5F5F5;
    padding: 15px;
    box-shadow: 0px 1px 1px 0px rgba(0, 0, 0, 0.2);
    border: solid 1px #EAEAEA;
  }

  .animation-element.slide-left.testimonial:hover,
  .animation-element.slide-left.testimonial:active {
    box-shadow: 0px 2px 8px 0px rgba(0, 0, 0, 0.25);
  }

  .animation-element.slide-left.testimonial:nth-of-type(odd) {
    width: 48.5%;
    margin: 0% 1.5% 3.0% 0%;
  }

  .animation-element.slide-left.testimonial:nth-of-type(even) {
    width: 48.5%;
    margin: 0% 0% 3.0% 1.5%;
  }

  .animation-element.slide-left.testimonial .header {
    float: left;
    width: 100%;
    margin-bottom: 10px;
  }

  .animation-element.slide-left.testimonial .left {
    float: left;
    margin-right: 15px;
  }

  .animation-element.slide-left.testimonial .right {
    float: left;
  }

  .animation-element.slide-left.testimonial img {
    width: 65px;
    height: 65px;
    border-radius: 50%;
    box-shadow: 0px 1px 3px rgba(51, 51, 51, 0.5);
  }

  .animation-element.slide-left.testimonial h3 {
    margin: 0px 0px 5px 0px;
  }

  .animation-element.slide-left.testimonial h4 {
    margin: 0px 0px 5px 0px;
  }

  .animation-element.slide-left.testimonial .content {
    float: left;
    width: 100%;
    margin-bottom: 10px;
  }

  .animation-element.slide-left.testimonial .rating {}

  .animation-element.slide-left.testimonial i {
    color: #aaa;
    margin-right: 5px;
  }



  /*media queries for small devices*/
  @media screen and (max-width: 678px) {

    /*testimonials*/
    .animation-element.slide-left.testimonial,
    .animation-element.slide-left.testimonial:nth-of-type(odd),
    .animation-element.slide-left.testimonial:nth-of-type(even) {
      width: 100%;
      margin: 0px 0px 20px 0px;
    }

    .animation-element.slide-left.testimonial .right,
    .animation-element.slide-left.testimonial .left,
    .animation-element.slide-left.testimonial .content,
    .animation-element.slide-left.testimonial .rating {
      text-align: center;
      float: none;
    }

    .animation-element.slide-left.testimonial img {
      width: 85px;
      height: 85px;
      margin-bottom: 5px;
    }

  }

  .menu-toggle .bar {
    width: 25px;
    height: 3px;
    background-color: #3f3f3f;
    margin: 5px auto;
    -webkit-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    display: block;
  }

  nav {
    width: 100%;
    background: #fdfdfd;
    float: left;
    position: sticky;
    top: 0;
    z-index: 99;
  }

  .mobile-nav {
    display: none;
    margin: 0;
    padding: 0;
    float: left;
    width: 100%;
    list-style: none;
    text-align: center;
  }

  .mobile-nav li {
    padding: 10px 0;
    color: #3e4348;
  }

  .menu-toggle {
    justify-self: end;
    margin-right: 25px;
    display: none;
    margin-top: 20px;
    float: right;
  }

  .menu-toggle:hover {
    cursor: pointer;
  }

  #mobile-menu.is-active .bar:nth-child(2) {
    opacity: 0;
  }

  #mobile-menu.is-active .bar:nth-child(1) {
    -webkit-transform: translateY(8px) rotate(45deg);
    -ms-transform: translateY(8px) rotate(45deg);
    -o-transform: translateY(8px) rotate(45deg);
    transform: translateY(8px) rotate(45deg);
  }

  #mobile-menu.is-active .bar:nth-child(3) {
    -webkit-transform: translateY(-8px) rotate(-45deg);
    -ms-transform: translateY(-8px) rotate(-45deg);
    -o-transform: translateY(-8px) rotate(-45deg);
    transform: translateY(-8px) rotate(-45deg);
  }


  @media only screen and (max-width: 600px) {

    .col-xs-6,
    .col-xs-3,
    .col-xs-4 {
      width: 100%;
    }

    nav .col-xs-6 {
      width: 50%;
    }

    .reverse-columns {
      display: flex;
      flex-direction: column-reverse;
    }

    .hero {
      width: 100%;
      top: unset;
      height: auto;
      padding: 20px 0;
      transform: none;
    }

    .hero-text {
      width: 100%;
      top: 0;
      text-align: center;
      float: none;
      margin-top: 20px;
      transform: none;
    }

    .hero-image {
      width: 100%;
      margin-top: 20px;
      transform: none;
      top: unset;
      float: none;
    }

    .hero-image img {
      width: 100%;
    }

    .offer-row {
      float: left;
      height: auto;
      padding: 20px 0;
      width: 100%;
      margin-bottom: 20px;
    }

    .offer-column {
      width: 100%;
      transform: none;
      top: unset;
    }

    .mobile-nav-height {
      height: 196px;
    }

    .navigation-left,
    .navigation-right {
      transform: none;
      top: unset;
    }

    .navigation-left {
      padding-top: 5px;
    }

    .navigation-right {
      margin-top: 15px;
    }

    .offer-column a {
      display: block;
      width: 80%;
      margin: 0 auto;
    }

    .column50 {
      width: 100%;
      text-align: center;
      transform: none !important;
      top: unset;
    }

    .menu-toggle {
      display: block;
    }

    .centre-image img {
      width: 100%;
    }

    .row {
      float: left;
      height: auto;
    }

    footer {
      height: auto;
      float: left;
    }

    .footer-row {
      width: 80%;
      padding: 0 10%;
    }

    .column25 {
      width: 100%;
      margin: 10px 0 5px 0;
    }

    .footer-top img {
      width: 50%;
    }

    .footer-column50 {
      width: 100%;
      text-align: center;
    }

    .nav-desktop {
      display: none !important;
    }

    .navigation-left,
    .navigation-right {
      width: 50%;
    }

    .hero-height {
      height: auto;
    }
  }

  .results {
    display: flex;
    max-width: 1000px;
    margin: 0 auto;
    flex-wrap: wrap;
  }

  .item {
    flex: 1 0 21%;
    /* explanation below */
    margin: 5px;
    height: auto;
    max-width: 270px;
    background-color: #F8F9FA;
  }

  .item .name {
    font-size: larger;
  }

  .item .price {
    color: #FF5F49;
    font-size: 14px;
  }

  .shop-thumbnail {
    width: 100%;
    height: 75%;
  }

  .hidden {
    display: none;
  }

  .visible {
    display: block;
  }

  .product-type-selection {
    display: none;
  }
</style>
@section('scripts')
<script type="text/javascript">
  $("#btn-search").click(function(e) {
    var search = $("#search").val();
    $.ajax({
      url: '{{ route('search') }}',
      method: "POST",
      data: {
        _token: '{{ csrf_token() }}',
        search: search
      },
      success: function(response) {
        console.log(response)
        // window.location.reload();
      }
    });
  });
</script>
@endsection