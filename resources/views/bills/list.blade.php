@extends('layouts.master')

@section('title','List bill')

@section('content')
<!-- <a href="{{url('/insertProduct')}}">+ Add Product</a> -->
<table class="table table-bordered">
  <tr class="table-primary">
    <th>ID</th>
    <th>Name</th>
    <th>Address</th>
    <th>Phone</th>
    <th>ID customer</th>
    <th colspan="2">Hành động</th>
  </tr>
  @forelse ($bills as $item)
  <tr data-id="{{ $item->id }}">
    <td>{{$item->id}}</td>
    <td>{{$item->name_customer}}</td>
    <td>
      <b>{{$item->address}}</b> <br>
    </td>
    <td>
      {{$item->phone_number}}
    </td>
    <td>
      {{$item->customer_id}}
    </td>
    <td>
      <div class="form-check">
        <!-- <input type="hidden" value="{{$item->id}}" id="order_id"> -->
        @if($item->is_set_up === 1)
        <input class="form-check-input" type="checkbox" value="{{$item->is_set_up}}" id="flexCheckIndeterminate" checked>
        @elseif($item->is_set_up === 0)
        <input class="form-check-input" type="checkbox" value="{{$item->is_set_up}}" id="flexCheckIndeterminate">
        @endif
        <label class="form-check-label" for="flexCheckIndeterminate">
          Order approval
        </label>
      </div>
    </td>
  </tr>
  @empty
  <tr>
    <td colspan="5" class="text-center">Danh sách trống</td>
  </tr>
  @endforelse

</table>
@endsection
@section('scripts')
<script type="text/javascript">
  $(".form-check-input").click(function(e) {
    e.preventDefault();

    var ele = $(this);
    $.ajax({
      url: '{{ route('update.bill') }}',
      method: "post",
      data: {
        _token: '{{ csrf_token() }}',
        id: ele.parents("tr").attr("data-id"),
        is_set_up: ele.parents("tr").find(".form-check-input").val()
      },
      success: function(response) {
        window.location.reload();
      }
    });
  });
</script>
@endsection