<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>@yield('title','Untitled')</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/styles/metro/notify-metro.css" />

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>

<body style="background-color: #EEE">
  <div class="container" style="background-color: white; min-height: 100vh;position: relative;">
    @section('header')
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container-fluid">
        @if(session('type_account') === 2)
        <a class="navbar-brand active" href="/">Home</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        @endif
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <!-- <a class="nav-link active" aria-current="page" href="/">Home</a> -->
            @if(session('type_account') === 1)
            <a class="nav-link" href="{{url('/brands')}}">Brands</a>
            <a class="nav-link" href="{{url('/products')}}">Products</a>
            <a class="nav-link" href="{{url('/bills')}}">Bills</a>
            @endif
            <a class="nav-link" href="{{url('/account/logout')}}">Logout</a>
          </div>
          <div class="main-section">
            <div class="dropdown">
              @if(session('type_account') === 2)
              <button type="button" class="btn btn-info" data-toggle="dropdown">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
              </button>
              <div class="dropdown-menu" style="width: 100vh">
                <div class="row total-header-section" style="height: 50px; padding: 10px 5%;">
                  <div class="col-lg-6 col-sm-6 col-6">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
                  </div>
                  @php $total = 0 @endphp
                  @foreach((array) session('cart') as $id => $details)
                  @php $total += $details['price'] * $details['quantity'] @endphp
                  @endforeach
                  <div class="col-lg-6 col-sm-6 col-6 total-section text-right">
                    <p>Total: <span class="text-info">đ {{ $total }}</span></p>
                  </div>
                </div>
                @if(session('cart'))
                @foreach(session('cart') as $id => $details)
                <div class="row cart-detail" style="height: 100px; padding: 10px 5%;">
                  <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
                    <img style="width: 90px;" src="{{ $details['image'] }}" />
                  </div>
                  <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                    <p>{{ $details['name'] }}</p>
                    <span class="price text-info"> đ{{ $details['price'] }}</span> <span class="count"> Quantity:{{ $details['quantity'] }}</span>
                  </div>
                </div>
                @endforeach
                @endif
                <div class="row">
                  <div class="col-lg-12 col-sm-12 col-12 text-center checkout">
                    <a href="{{ url('cart') }}" class="btn btn-primary btn-block">View all</a>
                  </div>
                </div>
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </nav>
    @show
    <br />
    <div class="container">
      @if(session('success'))
      <div class="alert alert-success">
        {{ session('success') }}
      </div>
      @endif

      @yield('content')
    </div>
    @yield('scripts')
    @section('footer')
    <div style="position: absolute;bottom:16px">
      <h5>Copyright &copy; BKACAD</h5>
    </div>
    @show
  </div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
</body>

</html>