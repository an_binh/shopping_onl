@extends('layouts.master')

@section('title','List products')

@section('content')
<a href="{{url('/insertProduct')}}">+ Add Product</a>
<table class="table table-bordered">
  <tr class="table-primary">
    <th>ID</th>
    <th>Product</th>
    <th>Price</th>
    <th>Quantity</th>
    <th colspan="2">Hành động</th>
  </tr>
  @forelse ($products as $item)
  <tr>
    <td>{{$item->id}}</td>
    <td>
      <b>{{$item->name}}</b> <br>
      <img src="{{asset($item->image)}}" width="200px">
    </td>
    <td>
      {{$item->price}}
    </td>
    <td>
      {{$item->quantity}}
    </td>
    <td>
      {{-- /edit/2 --}}
      <a href="{{url('/editProduct/'.$item->id)}}">Sửa</a>
    </td>
    <td>
      <a href="{{url('/deleteProduct/'.$item->id)}}">Xoá</a>
    </td>
  </tr>
  @empty
  <tr>
    <td colspan="5" class="text-center">Danh sách trống</td>
  </tr>
  @endforelse

</table>
@endsection