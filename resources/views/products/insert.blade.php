@extends('layouts.master')

@section('title','Thêm sản phẩm')

@section('content')
<h1 class="text-center display-4">Add Product</h1>
<form action="{{url('/insertProduct')}}" method="POST" enctype="multipart/form-data" style="width: 50%; margin: auto; text-align:center">
  @csrf
  <div class="input-group mb-3">
    <span class="input-group-text" id="basic-addon1">Name</span>
    <input required name="name" type="text" class="form-control" placeholder="Name product" aria-describedby="basic-addon1">
  </div>
  <div class="input-group mb-3">
    <span class="input-group-text" id="basic-addon2">Price</span>
    <input required name="price" type="number" step="any" class="form-control" placeholder="Price product" aria-describedby="basic-addon2">
  </div>
  <div class="input-group mb-3">
    <span class="input-group-text" id="basic-addon3">Quantity</span>
    <input required name="quantity" type="number" class="form-control" placeholder="Quantity product" aria-describedby="basic-addon3"></input>
  </div>
  <div class="input-group mb-3">
    {{-- <span class="input-group-text" id="basic-addon1">Image</span> --}}
    <input required name="image" type="file" class="form-control" accept="image/*">
  </div>
  <div class="mb-3">
    <select name="id_brand" class="form-select form-select-sm" aria-label=".form-select-sm example">
      <option selected>Open this select menu</option>
      @foreach ($brands as $brand)
      <option value="{{$brand->id}}">{{$brand->name}}</option>
      @endforeach
    </select>
  </div>
  <button class="btn btn-primary btn-lg">Insert</button>
</form>
@endsection