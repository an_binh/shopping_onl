@extends('layouts.master')

@section('title','Danh sách')

@section('content')
<a href="{{url('/insertBrand')}}">+ Add Brand</a>
<table class="table table-bordered">
  <tr class="table-primary">
    <th>ID</th>
    <th>Name</th>
    <th>Description</th>
    <th colspan="2">Actions</th>
  </tr>
  @forelse ($brands as $item)
  <tr>
    <td>{{$item->id}}</td>
    <td>
      <b>{{$item->name}}</b>
    </td>
    <td>
      {{$item->description}}
    </td>
    <td>
      {{-- /edit/2 --}}
      <a href="{{url('/editBrand/'.$item->id)}}">Sửa</a>
    </td>
    <td>
      <a href="{{url('/deleteBrand/'.$item->id)}}">Xoá</a>
    </td>
  </tr>
  @empty
  <tr>
    <td colspan="5" class="text-center">Danh sách trống</td>
  </tr>
  @endforelse

</table>
@endsection