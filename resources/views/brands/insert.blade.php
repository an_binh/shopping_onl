@extends('layouts.master')

@section('title','Add brand')

@section('content')
<h1 class="text-center display-4">Add Brand</h1>
<form action="{{url('/insertBrand')}}" method="POST" enctype="multipart/form-data" style="width: 50%; margin: auto; text-align:center">
  @csrf
  <div class="input-group mb-3">
    <span class="input-group-text" id="basic-addon1">Name</span>
    <input required name="name" type="text" class="form-control" placeholder="Name product" aria-describedby="basic-addon1">
  </div>
  <div class="input-group mb-3">
    <span class="input-group-text" id="basic-addon3">Description</span>
    <textarea required name="description" type="text" class="form-control" placeholder="Description" aria-describedby="basic-addon3"></textarea>
  </div>
  <button class="btn btn-primary btn-lg">Insert</button>
</form>
@endsection