<?php

namespace Database\Seeders;

use App\Models\Account;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Account::create(
            [
                'username'=>'abc@gmail.com',
                'password'=>bcrypt('bkacad123'),
                'type_account'=>2
            ]
        );
    }
}
