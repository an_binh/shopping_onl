<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\AccountLoginController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\CustomerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//login
Route::get('/account/login', [AccountLoginController::class, 'showLoginForm'])->name('account.login');
Route::post('/account/login', [AccountLoginController::class, 'login']);
Route::get('/account/logout', [AccountLoginController::class, 'logout'])->name('account.logout');

Route::get('/account', [AccountController::class, 'index'])->name('account.home');

// Hiển thị tất cả sản phẩm
// Route::get('/', [ProductController::class, 'index'])->name('admin');
Route::get('/products', [ProductController::class, 'getAll'])->name('admin');
Route::get('/insertProduct', [ProductController::class, 'viewInsert']);
Route::post('/insertProduct', [ProductController::class, 'insert']);
Route::get('/editProduct/{id}', [ProductController::class, 'edit']);
Route::post('/editProduct/{id}', [ProductController::class, 'update']);
Route::get('/deleteProduct/{id}', [ProductController::class, 'destroy']);

// Hiển thị tất cả nhan hang
Route::get('/brands', [BrandController::class, 'getAll']);
Route::get('/insertBrand', function () {
    return view('brands/insert');
});
Route::post('/insertBrand', [BrandController::class, 'insert']);
Route::get('/editBrand/{id}', [BrandController::class, 'edit']);
Route::post('/editBrand/{id}', [BrandController::class, 'update']);
Route::get('/deleteBrand/{id}', [BrandController::class, 'destroy']);

Route::get('cart', [ProductController::class, 'cart'])->name('cart');
Route::get('addToCart/{id}', [ProductController::class, 'addToCart'])->name('add.to.cart');
Route::patch('updateCart', [ProductController::class, 'updateCart'])->name('update.cart');
Route::delete('removeFromCart', [ProductController::class, 'remove'])->name('remove.from.cart');
Route::get('search', [ProductController::class, 'search'])->name('search');

Route::post('/order', [OrderController::class, 'payment'])->name('payment');
Route::get('/customer', [CustomerController::class, 'index'])->name('customer');
Route::get('/bills', [OrderController::class, 'getAll']);
Route::post('/updateBill', [OrderController::class, 'update'])->name('update.bill');


